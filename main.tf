#==============================================================
# Shared / vpc.tf
#==============================================================

# Create the Virtual Private Cloud that the rest of our 
# infratructure will be built in.

resource "aws_vpc" "vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags {
    Name        = "${var.stack_name}-${var.environment}-vpc"
    owner       = "${var.owner}"
    stack_name  = "${var.stack_name}"
    environment = "${var.environment}"
    created_by  = "terraform"
    department    = "${var.department}"
    division      = "${var.division}"
    branch        = "${var.branch}"
    activity_name = "${var.activity_name}"
    section       = "${var.section}"
    project       = "${var.project}"
    cost_code     = "${var.cost_code}"
  }
}

#--------------------------------------------------------------
# Internet Gateway
#--------------------------------------------------------------

# Create an internet gateway so the VPC can talk to the wider
# internet.

resource "aws_internet_gateway" "rds_igw" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name        = "${var.stack_name}-${var.environment}-vpc"
    owner       = "${var.owner}"
    stack_name  = "${var.stack_name}"
    environment = "${var.environment}"
    created_by  = "terraform"
    department    = "${var.department}"
    division      = "${var.division}"
    branch        = "${var.branch}"
    activity_name = "${var.activity_name}"
    section       = "${var.section}"
    project       = "${var.project}"
    cost_code     = "${var.cost_code}"
  }
}


#--------------------------------------------------------------
# Subnet
#--------------------------------------------------------------

#Create subnets and subnet group
#Create route table

resource "aws_subnet" "subnet_1" {
  vpc_id     = "${aws_vpc.vpc.id}"
  cidr_block = "10.0.1.0/24"
  availability_zone = "ap-southeast-2a"
  map_public_ip_on_launch = "True"

  tags {
    Name        = "${var.stack_name}-${var.environment}-subnet1"
    owner       = "${var.owner}"
    stack_name  = "${var.stack_name}"
    environment = "${var.environment}"
    created_by  = "terraform"
    department    = "${var.department}"
    division      = "${var.division}"
    branch        = "${var.branch}"
    activity_name = "${var.activity_name}"
    section       = "${var.section}"
    project       = "${var.project}"
    cost_code     = "${var.cost_code}"
  }
}

resource "aws_subnet" "subnet_2" {
  vpc_id     = "${aws_vpc.vpc.id}"
  cidr_block = "10.0.2.0/24"
  availability_zone = "ap-southeast-2b"
  map_public_ip_on_launch = "True"

  tags {
    Name        = "${var.stack_name}-${var.environment}-subnet2"
    owner       = "${var.owner}"
    stack_name  = "${var.stack_name}"
    environment = "${var.environment}"
    created_by  = "terraform"
    department    = "${var.department}"
    division      = "${var.division}"
    branch        = "${var.branch}"
    activity_name = "${var.activity_name}"
    section       = "${var.section}"
    project       = "${var.project}"
    cost_code     = "${var.cost_code}"
  }
}

#--------------------------------------------------------------
# DB Subnet Group
#--------------------------------------------------------------

# Subnet Group for the RDS

resource "aws_db_subnet_group" "sdedist_subnet_grp" {
  name       = "${var.stack_name}-${var.environment}-subnet_grp"
  subnet_ids = ["${aws_subnet.subnet_1.id}", "${aws_subnet.subnet_2.id}"]

  tags {
    Name        = "${var.stack_name}-${var.environment}-subnet_grp"
    owner       = "${var.owner}"
    stack_name  = "${var.stack_name}"
    environment = "${var.environment}"
    created_by  = "terraform"
    department    = "${var.department}"
    division      = "${var.division}"
    branch        = "${var.branch}"
    activity_name = "${var.activity_name}"
    section       = "${var.section}"
    project       = "${var.project}"
    cost_code     = "${var.cost_code}"
  }
}

#--------------------------------------------------------------
# Route Table
#--------------------------------------------------------------


resource "aws_route_table" "rds_rtbl" {
  vpc_id = "${aws_vpc.vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.rds_igw.id}"
  }


  tags {
    Name        = "${var.stack_name}-${var.environment}-rds_rtbl"
    owner       = "${var.owner}"
    stack_name  = "${var.stack_name}"
    environment = "${var.environment}"
    created_by  = "terraform"
    department    = "${var.department}"
    division      = "${var.division}"
    branch        = "${var.branch}"
    activity_name = "${var.activity_name}"
    section       = "${var.section}"
    project       = "${var.project}"
    cost_code     = "${var.cost_code}"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = "${aws_subnet.subnet_1.id}"
  route_table_id = "${aws_route_table.rds_rtbl.id}"
}

resource "aws_route_table_association" "b" {
  subnet_id      = "${aws_subnet.subnet_2.id}"
  route_table_id = "${aws_route_table.rds_rtbl.id}"
}


#--------------------------------------------------------------
# RDS Snapshot
#--------------------------------------------------------------

# Identify most recent snapshot


data "aws_db_snapshot" "rds_snapshot" {
  most_recent = true
  db_instance_identifier = "${var.stack_name}-${var.environment}-rds"
}


#--------------------------------------------------------------
# RDS
#--------------------------------------------------------------

# Create an RDS

resource "aws_db_instance" "sdedist_rds" {
  instance_class       = "db.t2.medium"
  db_subnet_group_name = "${aws_db_subnet_group.sdedist_subnet_grp.name}"
  snapshot_identifier  = "${data.aws_db_snapshot.rds_snapshot.id}"
  identifier           = "${var.stack_name}-${var.environment}-rds"
  skip_final_snapshot  = "true"
  publicly_accessible  = "true"
  vpc_security_group_ids = ["${aws_security_group.rds_sg.id}"]
  copy_tags_to_snapshot = "true"
  backup_retention_period = "7"
    
  tags {
    Name        = "${var.stack_name}-${var.environment}-rds"
    owner       = "${var.owner}"
    stack_name  = "${var.stack_name}"
    environment = "${var.environment}"
    created_by  = "terraform"
    department    = "${var.department}"
    division      = "${var.division}"
    branch        = "${var.branch}"
    activity_name = "${var.activity_name}"
    section       = "${var.section}"
    project       = "${var.project}"
    cost_code     = "${var.cost_code}"
  }
}

#--------------------------------------------------------------
# Route53
#--------------------------------------------------------------

# Add to a Route53 entry
# Gives RDS a human readable URL to connect to

resource "aws_route53_record" "rds_r53" {
  zone_id = "ZD6N8NDIS9S8"
  name    = "database.gis.ga.gov.au"
  type    = "CNAME"
  ttl     = "300"
  
  records = ["${aws_db_instance.sdedist_rds.address}"]

}


#--------------------------------------------------------------
# Security Groups
#--------------------------------------------------------------

# Create Security Group for RDS

resource "aws_security_group" "rds_sg" {
  name        = "${var.stack_name}-${var.environment}"
  description = "Allow RDS traffic"
  vpc_id      = "${aws_vpc.vpc.id}"

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "6"
#    cidr_blocks = ["192.104.44.129/32"]
#    GA IP address
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  
  tags {
    Name = "${var.stack_name}-${var.environment}-rds-sg"
    owner       = "${var.owner}"
    stack_name  = "${var.stack_name}"
    environment = "${var.environment}"
    created_by  = "terraform"
    department    = "${var.department}"
    division      = "${var.division}"
    branch        = "${var.branch}"
    activity_name = "${var.activity_name}"
    section       = "${var.section}"
    project       = "${var.project}"
    cost_code     = "${var.cost_code}"
  }
}